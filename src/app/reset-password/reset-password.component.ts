import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ApiUrl } from '../core/apiUrl';
import { StyleVariables } from '../core/theme/styleVariables.model';
import { HttpService } from '../services/http/http.service';
import { MessagingService } from '../services/messaging/messaging.service';
import { UtilityService } from '../services/utility/utility.service';
import { AppSettings } from '../shared/models/appSettings.model';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})

export class ResetPasswordComponent implements OnDestroy {

  settingsSubscription: Subscription;
  settings: AppSettings;

  style: StyleVariables;
  styleSubscription: Subscription;
  routeSubscription: Subscription;

  form: FormGroup;

  isLoading = false;
  showError = false;

  token: string;
  hideForm = false;

  constructor(
    private util: UtilityService,
    private fb: FormBuilder,
     private http: HttpService,
    private message: MessagingService,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.settingsSubscription = this.util.getSettings.subscribe((settings: AppSettings) => {
      if (!!settings) {
        this.settings = settings;
      }
    });
    this.styleSubscription = this.util.getStyles.subscribe(style => {
      this.style = style;
    });

    this.routeSubscription = this.route.queryParams
      .subscribe(params => {
          if (params.token) {
            this.token = params.token;
          }
      });
    this.makeForm();
  }

  makeForm() {
    this.form = this.fb.group({
      'password': ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20)])],
      'confirmPassword': ['', Validators.compose([Validators.required])],
      'token': [this.token , Validators.required]
    });
  }

  onSubmit(value) {

    this.showError = true;
    if (this.form.valid) {
      if (value.password == value.confirmPassword) {
        this.isLoading = true;
        delete value.confirmPassword;
        this.http.postData(ApiUrl.auth.resetPassword, value)
          .subscribe(response => {
            this.isLoading = false;
            if (!!response && response.data) {
              this.message.toast('success', `${this.translate.instant('Password Changed Successfully')}!`);
              // this.router.navigate(['']);
              this.showError = false;
              this.hideForm = true;
            }
          }, error => { this.isLoading = false; });
      } else {
        this.message.toast('info', this.translate.instant('New Password And Confirm Password Not Matched'));
      }
    }
  }

  ngOnDestroy() {
    this.settingsSubscription.unsubscribe();
    this.styleSubscription.unsubscribe();
  }

}
