import { GlobalVariable } from './../../../../../core/global';
import { WINDOW } from './../../../../../services/window/window.service';
import { DOCUMENT } from '@angular/common';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { UtilityService } from '../../../../../services/utility/utility.service';
import { ScriptService } from '../../../../../services/script/script.service';
import { StyleVariables } from '../../../../../core/theme/styleVariables.model';
import { Subscription } from 'rxjs';
import { ScriptModel } from '../../../../../shared/models/script.model';
import { PaymentBaseComponent } from '../payment-base.component';
import { HttpService } from '../../../../../services/http/http.service';
import { ApiUrl } from 'src/app/core/apiUrl';
import { MessagingService } from 'src/app/services/messaging/messaging.service';

declare var $: any;

@Component({
    selector: 'app-hyper-pay-gateway',
    templateUrl: './hyper-pay-gateway.component.html',
    styleUrls: ['./hyper-pay-gateway.component.scss']
})
export class HyperPayGatewayComponent extends PaymentBaseComponent implements OnInit, OnDestroy {


    style: StyleVariables;
    showAddCard: boolean = false;
    disabled: boolean = false;

    styleSubscription: Subscription;

    constructor(
        private utilService: UtilityService,
        private scriptService: ScriptService,
        @Inject(WINDOW) private window: Window,
        @Inject(DOCUMENT) private document,
        private http: HttpService,
        private message: MessagingService,

    ) {
        super();
        this.style = new StyleVariables();
    }

    ngOnInit() {
        this.styleSubscription = this.utilService.getStyles.subscribe((style: StyleVariables) => {
            this.style = style;
        })

        this.getPaymentUrl();
    }

    getPaymentUrl() {
        var data = {
            amount: this.order.amount,
            currency: GlobalVariable.CURRENCY_NAME
        }
        this.http.postData(ApiUrl.getHyperPaymentUrl, data).subscribe((res: any) => {
            if (res.status == 200) {
                this.onSuccess.emit({
                    'paymentGatewayId': 'hyperpay',
                    'token': 'hyperpay',
                    'waitForSuccess': true,
                    'paymentUrl': ''
                });
                this.initializeHyperPay(res);
            }
            else {
                this.message.toast("error", "Payment could not processed");
            }
        })
    }

    initializeHyperPay(res) {
        const scriptLink = this.document.createElement('script');
        scriptLink.id = `hyper-pay-checkout`;
        scriptLink.setAttribute("type", "text/javascript");
        scriptLink.setAttribute("src", res.data.baseUrl);
        this.document.body.appendChild(scriptLink);

        const form = this.document.createElement('form');
        form.id = `hyper-pay-checkout-form`;
        form.setAttribute("action", this.window.origin + "/success?gateway='hyperpay'");
        form.setAttribute("class", "paymentWidgets");
        form.setAttribute("data-brands", "VISA MASTER AMEX");

        const ele = document.getElementById('card-element');

        ele.appendChild(form);

        this.payFormSettings();
    }



    payFormSettings() {
        var wpwlOptions = {
            style: "card",
            onReady: function (e) {
                $('.wpwl-form-card').find('.wpwl-button-pay').on('click', function (e) {
                    this.validateHolder(e);
                });
            },
            onBeforeSubmitCard: function (e) {
                return this.validateHolder(e);
            }
        }
    }

    validateHolder(e) {
        var holder = $('.wpwl-control-cardHolder').val();
        if (holder.trim().length < 2) {
            $('.wpwl-control-cardHolder').addClass('wpwl-has-error').after('<div class="wpwl-hint wpwl-hint-cardHolderError">Invalid card holder</div>');
            return false;
        }
        return true;
    }


    ngOnDestroy(): void {
        if (this.styleSubscription) this.styleSubscription.unsubscribe();
    }

}
