import { UtilityService } from './../../../../../../../services/utility/utility.service';
import { StyleConstants } from './../../../../../../../core/theme/styleConstants.model';
import { StyleVariables } from './../../../../../../../core/theme/styleVariables.model';
import { AppSettings } from './../../../../../../../shared/models/appSettings.model';
import { trigger, transition, style, animate } from '@angular/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-food-supplier',
  templateUrl: './food-supplier.component.html',
  styleUrls: ['./food-supplier.component.scss'],
  animations: [
    trigger('fade', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate('500ms ease-in-out', style({ opacity: 1 }))
      ])
    ])
  ]
})
export class FoodSupplierComponent implements OnInit {

  @Input() supplier: any;
  @Input() darkTheme: boolean;

  @Input() settings: AppSettings;
  @Input() style: StyleVariables;
  @Input() isBranch: boolean = false;

  @Output() onFavourite: EventEmitter<any> = new EventEmitter<any>();

  btnStyle: StyleConstants;
  subCatId: Array<any> = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private util: UtilityService
  ) {
    this.style = new StyleVariables();
    this.btnStyle = new StyleConstants();
  }

  ngOnInit() {
    this.route.queryParams
      .subscribe(_params => {
        if (_params.f) {
          let params = this.util.decryptData(_params.f);
          this.subCatId = [params['subCatId']];
        }
      });
    if (this.settings.selected_template == 1 && this.settings.app_type == 1 && this.supplier.category && this.supplier.category.length) {
      let cat_names = [];
      (this.supplier.category).forEach(ct => {
        cat_names.push(ct.category_name)
      });
      this.supplier['category_names'] = (cat_names.splice(0, 4)).join(', ');
    }
    this.setBtnStyle();
    this.isOpen();
  }

  onBtnHover() {
    this.btnStyle = {
      backgroundColor: this.style.baseColor,
      color: '#ffffff',
      borderColor: this.style.baseColor,
      transition: '1s'
    }
  }

  setBtnStyle() {
    this.btnStyle = {
      color: this.style.baseColor,
      borderColor: this.style.baseColor
    }
  }

  productList(supplier) {
    if (this.settings.is_single_vendor && this.settings.selected_template == 1) {
      let cat_ids: Array<any> = [];
      if (supplier.category && supplier.category.length) {
        supplier["category"].forEach(element => {
          cat_ids.push(element.category_id);
        });
      }
      let seoValue = supplier.name;
      let param_obj = {};
      Object.assign(param_obj, this.util.handler);
      param_obj["supplierId"] = [supplier.id];
      param_obj["showSupplier"] = true;
      param_obj['branch_id'] = supplier.supplier_branch_id;
      param_obj['categoryId'] = cat_ids.join();
      param_obj['subCatId'] = this.subCatId;
      this.router.navigate(["products/product-listing", seoValue], {
        queryParams: { f: this.util.encryptData(param_obj) }
      });
    } else {
      let queryParams = {
        supplierId: supplier.id,
      }
      if (this.settings.branch_flow == 1 && supplier.is_multi_branch == 1) {
        queryParams['is_branches'] = 1;
        // queryParams['branchId'] = supplier.supplier_branch_id;
        // queryParams['branchName'] = supplier.supplier_branch_name;
      } else if (supplier.supplier_branch_id && supplier.supplier_branch_name) {
        queryParams['branchId'] = supplier.supplier_branch_id;
        queryParams['branchName'] = supplier.supplier_branch_name;
      }
      this.router.navigate(["products/listing"], {
        queryParams: queryParams
      });
    }
  }

  today = new Date();
  isOpen(){
    this.supplier.timing.forEach(item=>{
      let start = this.today.getFullYear()+'/'+(this.today.getMonth()+1)+'/'+this.today.getDate()+' '+item.start_time;
      let start_time = new Date(start);
      let end = this.today.getFullYear()+'/'+(this.today.getMonth()+1)+'/'+this.today.getDate()+' '+item.end_time;
      let end_time = new Date(end);
      if(item.week_id == this.today.getDay() 
      && item.is_open
      && this.today > start_time
      && this.today < end_time
      ){
        console.log(this.today > start_time,this.today,start_time);
        console.log(this.today < end_time,this.today , end_time);
        this.supplier.isOpen = true;
      }
    });
  }

}