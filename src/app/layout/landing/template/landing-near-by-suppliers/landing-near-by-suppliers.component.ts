import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
    selector: 'app-landing-near-by-suppliers',
    templateUrl: './landing-near-by-suppliers.component.html',
    styleUrls: ['./landing-near-by-suppliers.component.scss', '../landing-recommended-suppliers/landing-recommended-suppliers.component.scss']
})
export class LandingNearBySuppliersComponent implements OnInit {

    @Input()
    suppliers: Array<any> = [];

    @Output()
    onViewDetial: EventEmitter<any> = new EventEmitter<any>();

    constructor() { }

    ngOnInit() {
    }

}
