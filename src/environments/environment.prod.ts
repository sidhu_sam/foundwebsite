export const environment = {
  production: true,

  ONBOARDING_BASE_API_URL: 'https://api.halo-up.com/v1/common',

  BASE_API_URL: 'https://api.halo-up.com',

  AGENT_BASE_API_URL: 'https://api.halo-up.com',

  ONBOARDING_TRACK_BASE_URL: 'https://api.halo-up.com',
};
